# Trace Model
# name: MAP~[9]
# parameters: {'complexity': 2, 'config': 'random_25%_error'}
# corpus_name: SMOS (5_0)
# threshold_technique: n/a
# threshold: n/a
SMOS47.txt ServletShowTeachingDetails.java 0.7329020283062913
SMOS47.txt ServletComputateStatistics.java 0.16119626266807174
SMOS47.txt DeleteManagerException.java 0.7952025664778741
SMOS47.txt RegisterLine.java 0.164361093035151
SMOS47.txt ServletAddTeachingAsTeacher.java 0.1877907649122598
SMOS47.txt ServletShowUserClassroomForm.java 0.3595135706047199
SMOS47.txt Role.java 0.26278035577670067
SMOS47.txt ServletShowUserRoleForm.java 0.24809770845920398
SMOS47.txt display.java 0.158199605158915
SMOS47.txt ServletShowTeachingList.java 0.2136123570689759
SMOS47.txt ServletReportTeachings.java 0.26219700155500325
# corpus_name: SMOS (5_0)
SMOS47.txt ServletShowTeachingDetails.java 0.7329020283062913
SMOS47.txt ServletComputateStatistics.java 0.16119626266807174
SMOS47.txt DeleteManagerException.java 0.7952025664778741
SMOS47.txt RegisterLine.java 0.164361093035151
SMOS47.txt ServletAddTeachingAsTeacher.java 0.1877907649122598
SMOS47.txt ServletShowUserClassroomForm.java 0.3595135706047199
SMOS47.txt Role.java 0.26278035577670067
SMOS47.txt ServletShowUserRoleForm.java 0.24809770845920398
SMOS47.txt display.java 0.158199605158915
SMOS47.txt ServletShowTeachingList.java 0.2136123570689759
SMOS47.txt ServletReportTeachings.java 0.26219700155500325
# corpus_name: SMOS (5_0)
SMOS41.txt ManagerRegister.java 0.9358053079958167
SMOS41.txt DeleteManagerException.java 0.27040723791566684
SMOS41.txt ServletInsertTeaching.java 0.26499196133238684
SMOS41.txt ServletShowUserTeachingFormByCourse.java 0.8080857644962846
SMOS41.txt ServletShowNoteList.java 0.8412567194032463
SMOS41.txt Report.java 0.17278571203007895
SMOS41.txt ServletUpdateTeaching.java 0.29644761238611034
SMOS41.txt ServletInitialize.java 0.36652514088151095
# corpus_name: SMOS (5_0)
SMOS65.txt DuplicatedEntityException.java 0.6014157406793542
SMOS65.txt ServletLoadReport.java 0.6866465415912794
SMOS65.txt ServletShowUserTeachingFormByCourse.java 0.2814384152877041
SMOS65.txt ServletDeleteTeaching.java 0.1787428346033652
SMOS65.txt User.java 0.17790997659987945
SMOS65.txt ConnectionException.java 0.001212631539700999
SMOS65.txt ServletAssignStudentClassroom.java 0.23917646587920713
SMOS65.txt ServletShowUserList.java 0.7593186783426111
SMOS65.txt ServletInsertUser.java 0.21039140799661138
# corpus_name: SMOS (5_0)
SMOS21.txt ServletDeleteTeaching.java 0.6689269499101479
SMOS21.txt ServletShowAddressDetails.java 0.7370726743866272
SMOS21.txt DuplicatedEntityException.java 0.20013386929898103
SMOS21.txt MandatoryFieldException.java 0.6701663608571161
SMOS21.txt RegisterLine.java 0.13605368415241337
SMOS21.txt ServletInsertReport.java 0.13817092754684562
SMOS21.txt ServletDeleteAddress.java 0.7394542934042607
SMOS21.txt Environment.java 0.022489775946762356
SMOS21.txt DeleteAdministratorException.java 0.20872494637684563
SMOS21.txt ConnectionPoolDataSource.java 0.8212642258950771
SMOS21.txt ServletAddressTeachings.java 0.3497237823134027
SMOS21.txt ServletDeleteJustify.java 0.1377209719766945
# corpus_name: SMOS (5_0)
SMOS31.txt ServletShowAddressDetails.java 0.22462556863933028
SMOS31.txt ServletShowClassroomList.java 0.6668423015447137
SMOS31.txt ServletShowReports.java 0.662747737020915
SMOS31.txt ServletUpdateReport.java 0.16127709198300041
SMOS31.txt ServletLogin.java 0.09906241915139716
SMOS31.txt ServletInitialize.java 0.35712151012689997
SMOS31.txt ManagerClassroom.java 0.3108990490602707
SMOS31.txt ServletDeleteUser.java 0.2900618467164684
# corpus_name: SMOS (5_0)
SMOS43.txt ServletInsertAddress.java 0.2507865937797654
SMOS43.txt ServletLogin.java 0.7573890437200916
SMOS43.txt ServletShowNoteDetails.java 0.9084285041882229
SMOS43.txt ServletShowJustifyList.java 0.8138990831093167
SMOS43.txt ServletInsertNewNote.java 0.8825045594297319
SMOS43.txt ServletShowReports.java 0.32311441004151464
SMOS43.txt ServletAddRemoveTeachingsAsAddress.java 0.2871962340835686
# corpus_name: SMOS (5_0)
SMOS53.txt MailUtility.java 0.18503699592953968
SMOS53.txt ManagerRegister.java 0.7824720214521699
SMOS53.txt LoginException.java 0.266632634571861
SMOS53.txt ServletShowClassroomList.java 0.6749844163172628
SMOS53.txt ManagerClassroom.java 0.3865504043553188
SMOS53.txt EntityNotFoundException.java 0.8277166441361081
SMOS53.txt ServletShowReports.java 0.2197966025250311
SMOS53.txt ServletShowUserList.java 0.05069583738061719
SMOS53.txt ManagerTeaching.java 0.09315491955431512
# corpus_name: SMOS (5_0)
SMOS26.txt ManagerAddress.java 0.7607454710876307
SMOS26.txt ServletUpdateRegister.java 0.21041754835142804
SMOS26.txt ServletShowUserRoleForm.java 0.6915300926920622
SMOS26.txt ServletUpdateTeaching.java 0.7421537350772383
SMOS26.txt Address.java 0.21289617515927012
SMOS26.txt ServletUpdateUser.java 0.18410307426942
SMOS26.txt ServletShowStudentsByClass.java 0.1715083539207103
SMOS26.txt ServletShowNoteDetails.java 0.13668069827806545
SMOS26.txt ServletDeleteUser.java 0.32797220044919834
SMOS26.txt ServletInitialize.java 0.3013029038674399
# corpus_name: SMOS (5_0)
SMOS59.txt Role.java 0.2826934441415413
SMOS59.txt managerUser.java 0.3262590683422952
SMOS59.txt ServletAssignParentStudent.java 0.06865453329626572
SMOS59.txt PermissionException.java 0.17597153627867565
SMOS59.txt Utility.java 0.19212082391656563
SMOS59.txt ServletComputateStatistics.java 0.259709893363721
SMOS59.txt Delay.java 0.0039004554856259576
SMOS59.txt ServletAddTeachingAsTeacher.java 0.15231432255629807
SMOS59.txt ServletUpdateJustify.java 0.6517106550868798
# corpus_name: SMOS (5_0)
SMOS37.txt DeleteAdministratorException.java 0.2750519706807089
SMOS37.txt ServletAssignParentStudent.java 0.2793075070556329
SMOS37.txt ServletDeleteJustify.java 0.7684472450618106
SMOS37.txt ConnectionWrapper.java 0.7346740702160365
SMOS37.txt MailUtility.java 0.37188238698576254
SMOS37.txt ServletLoadClassByAccademicYear.java 0.8178191015674741
SMOS37.txt DeleteManagerException.java 0.1628306164871349
SMOS37.txt ServletAddTeachingAsTeacher.java 0.31965440601094103
SMOS37.txt ServletDeleteUser.java 0.36019937845775707
SMOS37.txt ServletLoadTeachingList.java 0.7970580794054737
SMOS37.txt ServletShowAddressDetails.java 0.8079042015967571
SMOS37.txt ManagerClassroom.java 0.4725402797467649
SMOS37.txt EntityNotFoundException.java 0.41493165948428395
SMOS37.txt Utility.java 0.41942907017606434
SMOS37.txt ServletShowNoteDetails.java 0.8964201240946726
SMOS37.txt LoginException.java 0.21755405516221754
# corpus_name: SMOS (5_0)
SMOS24.txt ServletInsertReport.java 0.14611470739283944
SMOS24.txt ServletInsertUser.java 0.23088175431887703
SMOS24.txt display.java 0.14080207626090702
SMOS24.txt ServletUpdateTeaching.java 0.7570640344703142
SMOS24.txt ServletDeleteJustify.java 0.1675177068468601
SMOS24.txt ServletShowJustifyList.java 0.9998147871887654
SMOS24.txt ServletShowUserTeachingFormByCourse.java 0.24039942088180194
SMOS24.txt ServletShowAddressList.java 0.7246274104832263
SMOS24.txt DuplicatedEntityException.java 0.6981922054444367
SMOS24.txt ServletDeleteUser.java 0.2784552889530183
# corpus_name: SMOS (5_0)
SMOS67.txt Utility.java 0.297624901322931
SMOS67.txt PermissionException.java 0.21862505835541357
SMOS67.txt EntityNotFoundException.java 0.18296265922521548
SMOS67.txt ServletShowClassroomByAcademicYear.java 0.25630365340873507
SMOS67.txt ManagerAddress.java 0.22001287307661638
SMOS67.txt ServletShowJustifyList.java 0.7991081028682296
SMOS67.txt ServletShowClassroomList.java 0.7562779812618396
SMOS67.txt MandatoryFieldException.java 0.2275572516035561
# corpus_name: SMOS (5_0)
SMOS52.txt ServletShowClassroomList.java 0.2548067579409199
SMOS52.txt ServletInsertAddress.java 0.30966858146735043
SMOS52.txt ServletAssignParentStudent.java 0.22856997453662906
SMOS52.txt ServletInsertTeaching.java 0.2747930336947021
SMOS52.txt ServletLogout.java 0.7019079107046123
SMOS52.txt Address.java 0.881876497636226
SMOS52.txt TestRegister.java 0.2661401627162967
SMOS52.txt ServletComputateStatistics.java 0.19496563232602784
SMOS52.txt ConnectionPoolDataSource.java 0.45901380995367375
# corpus_name: SMOS (5_0)
SMOS5.txt Absence.java 0.13705880203779428
SMOS5.txt ServletInsertNewNote.java 0.1305728993376758
SMOS5.txt NotImplementedYetException.java 0.6659966820598716
SMOS5.txt ServletShowUserTeachingForm.java 0.657609998084404
SMOS5.txt Justify.java 0.1360430696003234
SMOS5.txt DefaultException.java 0.27260837653293457
SMOS5.txt ServletAddRemoveTeachingsAsAddress.java 0.18187082819293676
# corpus_name: SMOS (5_0)
SMOS28.txt ServletLogin.java 0.6763168350070454
SMOS28.txt ServletInsertReport.java 0.18543290186692404
SMOS28.txt ServletDeleteAddress.java 0.21203240789993846
SMOS28.txt ServletUpdateTeaching.java 0.9955243664125549
SMOS28.txt DefaultException.java 0.4106376195314103
SMOS28.txt ServletDeleteJustify.java 0.689073449424297
SMOS28.txt ServletDeleteClassroom.java 0.20443275141220438
SMOS28.txt ServletUpdateUser.java 0.19439124307096395
# corpus_name: SMOS (5_0)
SMOS16.txt ServletInsertReport.java 0.14798477018289777
SMOS16.txt ServletShowUserRoleForm.java 0.18967135605804153
SMOS16.txt ServletLoadReport.java 0.01161794032638872
SMOS16.txt display.java 0.14356904399599615
SMOS16.txt ServletUpdateTeaching.java 0.6482925912626709
SMOS16.txt ServletShowNoteList.java 0.6442258720085977
SMOS16.txt ServletLogout.java 0.6873640322859025
SMOS16.txt ServletUpdateUser.java 0.9028281173781305
SMOS16.txt ServletShowUserTeachingFormByCourse.java 0.23656429787282585
SMOS16.txt ServletShowClassroomManagement.java 0.18362279160703882
SMOS16.txt ServletDeleteNote.java 0.6449689319664862
SMOS16.txt ServletInsertAddress.java 0.6696857603925572
# corpus_name: SMOS (5_0)
SMOS32.txt ServletShowUserTeachingForm.java 0.18475175900690136
SMOS32.txt ServletShowUserList.java 0.19743493037116838
SMOS32.txt ServletLoadReport.java 0.1744703800262252
SMOS32.txt ServletUpdateUser.java 0.25777290842016565
SMOS32.txt ServletLoadClassByAccademicYear.java 0.7455637247626237
SMOS32.txt ServletDeleteReport.java 0.16598428983571542
SMOS32.txt Delay.java 0.43749880960257187
SMOS32.txt Role.java 0.2349227908788119
SMOS32.txt LoginException.java 0.18433467629351716
SMOS32.txt ServletShowAddressList.java 0.6815419336849855
SMOS32.txt ServletShowTeachingDetails.java 0.2437375962961325
SMOS32.txt ServletShowUserClassroomForm.java 0.23691529133186748
# corpus_name: SMOS (5_0)
SMOS39.txt EntityNotFoundException.java 0.43446470328416015
SMOS39.txt ServletUpdateTeaching.java 0.2680549464810424
SMOS39.txt ServletInsertReport.java 0.2812558213132993
SMOS39.txt Teaching.java 0.19631660388064928
SMOS39.txt DeleteManagerException.java 0.28135012537023
SMOS39.txt ServletLoadYear.java 0.8542936069146712
SMOS39.txt DBConnection.java 0.7154081477696254
SMOS39.txt ServletShowRegister.java 0.8370540799203198
SMOS39.txt ManagerTeaching.java 0.424946135063385
SMOS39.txt ServletAddressTeachings.java 0.38351064170223476
# corpus_name: SMOS (5_0)
SMOS15.txt ServletLoadReport.java 0.14598247323827115
SMOS15.txt ServletShowReports.java 0.7014080895892983
SMOS15.txt ServletAssignStudentClassroom.java 0.26780466442051504
SMOS15.txt ServletShowUserDetails.java 0.2263704768536842
SMOS15.txt ServletInsertTeaching.java 0.6465744583544145
SMOS15.txt ServletShowJustifyDetails.java 0.31610595871462455
SMOS15.txt ServletDeleteReport.java 0.27344523645294944
SMOS15.txt MailUtility.java 0.23683326200288027
# corpus_name: SMOS (5_0)
SMOS63.txt Votes.java 0.1369938221573923
SMOS63.txt ServletLoadTeachingList.java 0.2844439198467414
SMOS63.txt managerUser.java 0.3754425207540799
SMOS63.txt Address.java 0.1847882282138862
SMOS63.txt ServletRemoveTeachingAsTeacher.java 0.25023765648553714
SMOS63.txt ServletShowClassroomList.java 0.22633512753922017
SMOS63.txt Utility.java 0.8490197735675105
SMOS63.txt ServletDeleteNote.java 0.3195399385597549
# corpus_name: SMOS (5_0)
SMOS62.txt ServletAssignParentStudent.java 0.739767804930035
SMOS62.txt Role.java 0.2549869202393383
SMOS62.txt ConnectionException.java 0.2107471148437761
SMOS62.txt DeleteAdministratorException.java 0.2972208461040249
SMOS62.txt ServletUpdateTeaching.java 0.1543734606583857
# corpus_name: SMOS (5_0)
SMOS17.txt ConnectionPoolDataSource.java 0.29829356404032265
SMOS17.txt managerUser.java 0.40140627136121615
SMOS17.txt ServletShowUserDetails.java 0.6580548650588703
SMOS17.txt ServletShowClassroomDetails.java 0.7112634338128911
SMOS17.txt EntityNotFoundException.java 0.1497242047727903
SMOS17.txt ServletUpdateReport.java 0.0909709718155772
SMOS17.txt Justify.java 0.13622909746857959
SMOS17.txt Environment.java 0.16246775220594967
# corpus_name: SMOS (5_0)
SMOS40.txt ServletShowTeachingDetails.java 0.7123632355927102
SMOS40.txt ManagerClassroom.java 0.20708023840863213
SMOS40.txt ServletInsertUser.java 0.9995744523294899
SMOS40.txt ServletAlterPersonalDate.java 0.24255119204133924
SMOS40.txt DuplicatedEntityException.java 0.001808062543386331
SMOS40.txt ServletShowClassroomList.java 0.16537372895525063
SMOS40.txt ServletShowReports.java 5.472840131832355e-08
SMOS40.txt ServletInsertAddress.java 0.31153863184103564
SMOS40.txt ServletLoadYear.java 0.1857963211028161
SMOS40.txt DeleteManagerException.java 0.2372272069492733
SMOS40.txt ServletShowStudentsByClass.java 0.18024604935487903
SMOS40.txt Classroom.java 0.63141404309847
SMOS40.txt Utility.java 0.1457977871743408
# corpus_name: SMOS (5_0)
SMOS20.txt Justify.java 0.004073718645987796
SMOS20.txt Absence.java 0.028998556839234454
SMOS20.txt Note.java 0.1618625325305255
SMOS20.txt Votes.java 0.17451702906879463
SMOS20.txt Report.java 0.2731966540650118
SMOS20.txt ServletShowTeachingList.java 0.7224203157630842
SMOS20.txt managerUser.java 0.34575956503255223
SMOS20.txt ServletDeleteAddress.java 0.8491917012328523
SMOS20.txt DuplicatedEntityException.java 0.39085210729638326
SMOS20.txt RegisterLine.java 0.18149284440978652
SMOS20.txt ServletShowAddressDetails.java 0.7957066744547912
SMOS20.txt ServletShowRegister.java 0.7205306968271276
SMOS20.txt ServletDeleteJustify.java 0.2550223442837738
SMOS20.txt ServletShowUserTeachingFormByCourse.java 0.004855398821882916
SMOS20.txt PermissionException.java 0.40318005292260395
SMOS20.txt ServletAddRemoveTeachingsAsAddress.java 0.6944492649441038
# corpus_name: SMOS (5_0)
SMOS13.txt ManagerAddress.java 0.17837466506654887
SMOS13.txt ConnectionException.java 0.2519886283174101
SMOS13.txt ServletShowUserList.java 0.6923716900879118
SMOS13.txt display.java 0.1416115225395279
SMOS13.txt DeleteManagerException.java 0.3184716324028748
SMOS13.txt ServletShowUserTeachingForm.java 0.6519258129877084
SMOS13.txt ServletShowAddressDetails.java 0.6475092085583234
# corpus_name: SMOS (5_0)
SMOS2.txt ServletShowTeachingList.java 0.20046988967488952
SMOS2.txt ServletLoadClassByAccademicYear.java 0.21959732718634817
SMOS2.txt ConnectionWrapper.java 0.7723798632093946
SMOS2.txt EntityNotFoundException.java 0.26648810058579664
SMOS2.txt ServletShowJustifyDetails.java 0.6448056771184555
SMOS2.txt ServletShowUserDetails.java 0.18850357998193826
SMOS2.txt ServletShowUserRoleForm.java 0.24915854519844338
SMOS2.txt ServletLoadYear.java 0.2057814028921094
SMOS2.txt ServletDeleteJustify.java 0.2677693852803791
SMOS2.txt ManagerVotes.java 0.03780972961807714
# corpus_name: SMOS (5_0)
SMOS6.txt ServletShowUserList.java 0.857747564677262
SMOS6.txt ManagerTeaching.java 0.304201064803696
SMOS6.txt ConnectionWrapper.java 0.8790524896932191
SMOS6.txt DeleteAdministratorException.java 0.41313083466814576
SMOS6.txt ServletLogout.java 0.2349223876293155
SMOS6.txt ServletShowJustifyDetails.java 0.7869858306053147
SMOS6.txt ServletUpdateReport.java 0.28080098814216986
SMOS6.txt ServletRemoveStudentClassroom.java 0.7872167110297221
SMOS6.txt ServletComputateStatistics.java 0.2902121021845082
SMOS6.txt ServletShowTeacherTeachingFormByClass.java 0.8861532804938971
# corpus_name: SMOS (5_0)
SMOS1.txt ServletInsertAddress.java 0.7664230915654152
SMOS1.txt RegisterLine.java 0.30404349825618737
SMOS1.txt ServletLogout.java 0.7428685494812095
SMOS1.txt PermissionException.java 0.3134613562654541
SMOS1.txt Justify.java 0.16074303959331562
SMOS1.txt DefaultException.java 0.7492250968446721
SMOS1.txt UserTest.java 0.17379388883019778
SMOS1.txt Teaching.java 0.7043758697158886
SMOS1.txt Note.java 0.053222228573408054
SMOS1.txt ServletLoadClassByAccademicYear.java 0.20971503012106002
SMOS1.txt ServletShowUserRoleForm.java 0.23883819604150053
SMOS1.txt ServletShowStudentsByClass.java 0.20421469528555014
SMOS1.txt ServletDeleteUser.java 0.3842463287125729
# corpus_name: SMOS (5_0)
SMOS57.txt Absence.java 0.9029223058277359
SMOS57.txt ServletLoadTeachingList.java 0.24511142003051392
SMOS57.txt ServletDeleteJustify.java 0.1834993367709184
SMOS57.txt ServletRemoveTeachingAsTeacher.java 0.7428065748048702
SMOS57.txt ServletShowNoteList.java 0.6868919148702709
SMOS57.txt DeleteManagerException.java 0.3281588157815805
SMOS57.txt ServletInitialize.java 0.3202564668804572
SMOS57.txt UserTest.java 0.21903111542670797
# corpus_name: SMOS (5_0)
SMOS64.txt ServletAlterPersonalDate.java 0.6604941596947604
SMOS64.txt ServletLoadYear.java 0.26312923423980705
SMOS64.txt ServletInitialize.java 0.001454264442506479
SMOS64.txt MandatoryFieldException.java 0.23094710757924655
SMOS64.txt Teaching.java 0.23912693333202217
SMOS64.txt EntityNotFoundException.java 0.33385294711263636
SMOS64.txt ServletDeleteAddress.java 0.16304654003385555
SMOS64.txt Address.java 0.8031433851902727
SMOS64.txt Classroom.java 0.4498075810496404
SMOS64.txt ServletShowJustifyList.java 7.274903891343275e-08
SMOS64.txt ServletShowNoteDetails.java 0.13566037954688367
SMOS64.txt managerUser.java 0.06336242444251289
# corpus_name: SMOS (5_0)
SMOS66.txt ServletInitialize.java 0.3313363239650351
SMOS66.txt Justify.java 0.7354694282962152
SMOS66.txt Teaching.java 0.2594754831725299
SMOS66.txt ServletShowClassroomDetails.java 0.18407189648911904
SMOS66.txt LoginException.java 0.24536198245598775
SMOS66.txt display.java 0.1406638950949309
SMOS66.txt ServletAddRemoveTeachingsAsAddress.java 0.18396657872628744
SMOS66.txt ServletUpdateJustify.java 2.21815994251878e-06
SMOS66.txt ServletAssignRole.java 0.1976460375232264
# corpus_name: SMOS (5_0)
SMOS61.txt ServletInsertNewNote.java 0.2769248958608626
SMOS61.txt UserListItem.java 0.2401226465398444
SMOS61.txt ManagerTeaching.java 0.3491052869731383
SMOS61.txt ServletReportTeachings.java 0.8079682442825022
SMOS61.txt ServletDeleteTeaching.java 0.17501142184325164
# corpus_name: SMOS (5_0)
SMOS35.txt Note.java 0.9701013831195439
SMOS35.txt ServletDeleteJustify.java 0.2671897868183006
SMOS35.txt ServletDeleteClassroom.java 0.26817515336744013
SMOS35.txt Utility.java 0.28528981870750314
SMOS35.txt AdaptorException.java 0.25294511336805786
SMOS35.txt ServletShowAddressDetails.java 0.29504634378140043
SMOS35.txt ServletProva.java 0.24804909750055248
# corpus_name: SMOS (5_0)
SMOS29.txt ServletShowUserList.java 0.21920728260329295
SMOS29.txt ServletShowNoteDetails.java 0.7056586036840109
SMOS29.txt ServletComputateStatistics.java 0.21903932875275547
SMOS29.txt ServletAssignRole.java 0.33347935747463875
SMOS29.txt ServletInsertClassroom.java 0.2414009724581378
SMOS29.txt ServletShowAddressDetails.java 0.691392501840152
SMOS29.txt ServletShowAddressList.java 0.20688130205593366
SMOS29.txt ServletShowJustifyList.java 0.6776793068752363
SMOS29.txt ManagerTeaching.java 0.3263083067596066
SMOS29.txt Utility.java 0.4327345269310386
SMOS29.txt DeleteAdministratorException.java 0.8148503685263693
SMOS29.txt Report.java 0.22404151497867555
# corpus_name: SMOS (5_0)
SMOS60.txt managerUser.java 0.39864033022600376
SMOS60.txt ServletShowUserRoleForm.java 0.3983714289573456
SMOS60.txt ServletShowUserClassroomForm.java 0.0038169707853424797
SMOS60.txt ServletLogin.java 0.2417648184450637
SMOS60.txt ManagerAddress.java 0.29397748487132186
SMOS60.txt ServletDeleteTeaching.java 0.042316174004584026
SMOS60.txt MailUtility.java 0.1932604607279264
SMOS60.txt ServletDeleteAddress.java 0.20102793560042528
SMOS60.txt ManagerClassroom.java 0.4466595907395965
SMOS60.txt ServletShowClassroomList.java 0.2263752776537681
SMOS60.txt DeleteAdministratorException.java 0.2568405282523837
SMOS60.txt ServletInsertTeaching.java 0.20978186308140404
SMOS60.txt Delay.java 0.4812510234574349
SMOS60.txt ServletShowClassroomDetails.java 0.27952639230937343
# corpus_name: SMOS (5_0)
SMOS33.txt ServletInsertTeaching.java 0.6788453550724562
SMOS33.txt ServletDeleteJustify.java 0.6718017500106932
SMOS33.txt ServletUpdateUser.java 0.01679446166403093
SMOS33.txt ServletShowAddressList.java 0.176076923626076
SMOS33.txt MailUtility.java 0.20322417807116355
SMOS33.txt managerUser.java 0.2849983497129609
SMOS33.txt Justify.java 0.7736773511009475
SMOS33.txt ServletComputateStatistics.java 0.6979555281895428
SMOS33.txt ServletRemoveTeachingAsTeacher.java 0.16969341621668696
SMOS33.txt DuplicatedEntityException.java 0.05488666092752197
SMOS33.txt ServletShowClassroomList.java 0.20036343605073148
SMOS33.txt Environment.java 0.15134398143435224
SMOS33.txt ServletShowUserTeachingForm.java 0.17386777988677438
# corpus_name: SMOS (5_0)
SMOS9.txt LoginException.java 0.2317585981575994
SMOS9.txt Votes.java 0.13176882348549818
SMOS9.txt MandatoryFieldException.java 0.6503829242755715
SMOS9.txt DeleteAdministratorException.java 0.4263404522144098
SMOS9.txt ConnectionPoolDataSource.java 0.26715874945261897
SMOS9.txt ServletUpdateJustify.java 0.15881877508346695
SMOS9.txt ServletComputateStatistics.java 0.13657145458314474
# corpus_name: SMOS (5_0)
SMOS58.txt Role.java 0.3088825742184227
SMOS58.txt ServletShowUserTeachingForm.java 0.19575091368225853
SMOS58.txt TestRegister.java 0.21027274240120006
SMOS58.txt Justify.java 0.7639553535578044
SMOS58.txt Report.java 0.46203888866100906
SMOS58.txt managerUser.java 0.3320618961661305
SMOS58.txt ServletDeleteTeaching.java 0.15877661155286082
SMOS58.txt ServletUpdateRegister.java 0.7977250154318447
SMOS58.txt ServletDeleteUser.java 0.19375185721236615
SMOS58.txt ServletUpdateUser.java 0.1579359411692167
# corpus_name: SMOS (5_0)
SMOS46.txt ServletUpdateTeaching.java 0.6530122410300613
SMOS46.txt ServletReportTeachings.java 0.31101998564713945
SMOS46.txt managerUser.java 0.2958655550447027
SMOS46.txt ServletLoadClassByAccademicYear.java 0.8244939730137917
SMOS46.txt ServletShowTeachingDetails.java 0.10002574243071745
SMOS46.txt PermissionException.java 0.2806663948142168
SMOS46.txt Report.java 0.4900118300363737
SMOS46.txt ServletAlterPersonalDate.java 0.213630116079054
SMOS46.txt ServletShowStudentsByClass.java 0.780724226173303
SMOS46.txt MailUtility.java 0.7390678247007573
SMOS46.txt ServletDeleteClassroom.java 0.20206860873199453
SMOS46.txt RegisterLine.java 0.28750259943388273
SMOS46.txt ServletUpdateJustify.java 0.14584170815799014
# corpus_name: SMOS (5_0)
SMOS18.txt ServletShowUserTeachingForm.java 0.8032289377839408
SMOS18.txt Teaching.java 0.6668068890080581
SMOS18.txt ServletAddRemoveTeachingsAsAddress.java 0.0012694843390015857
SMOS18.txt ServletShowNoteDetails.java 0.2587629334274757
SMOS18.txt UserTest.java 0.17158938996809317
SMOS18.txt ServletShowTeacherTeachingFormByClass.java 0.3553621565769914
SMOS18.txt DefaultException.java 0.0018574287017618111
SMOS18.txt ServletShowStudentsByClass.java 0.28492205626670547
SMOS18.txt ServletShowUserDetails.java 0.3105717818693815
SMOS18.txt TestRegister.java 0.3451799201187927
# corpus_name: SMOS (5_0)
SMOS36.txt Environment.java 0.15195345058577747
SMOS36.txt ServletDeleteClassroom.java 0.17980565917117447
SMOS36.txt AdaptorException.java 0.3147096659397452
SMOS36.txt ServletAssignRole.java 0.7636779862262074
SMOS36.txt ServletShowUserDetails.java 0.1922956023236271
SMOS36.txt ControlConnection.java 0.35007429336312323
SMOS36.txt ServletShowUserTeachingForm.java 0.2008762032902937
# corpus_name: SMOS (5_0)
SMOS23.txt Utility.java 0.3674657271902108
SMOS23.txt ServletDeleteAddress.java 0.8161500926675797
SMOS23.txt DefaultException.java 0.8003055699095822
SMOS23.txt EntityNotFoundException.java 0.28108529269808713
SMOS23.txt Votes.java 0.20169615168711014
SMOS23.txt ManagerTeaching.java 0.8680443345670268
SMOS23.txt ManagerRegister.java 0.31217576654490836
SMOS23.txt Role.java 0.2604738636615313
SMOS23.txt ServletUpdateJustify.java 0.33053300610779623
SMOS23.txt ServletLoadClassByAccademicYear.java 0.8249034080580678
SMOS23.txt ServletLoadTeachingList.java 0.34671318717471106
# corpus_name: SMOS (5_0)
SMOS38.txt ServletRemoveTeachingAsTeacher.java 0.15965052354598852
SMOS38.txt ServletShowUserList.java 0.2140686655927774
SMOS38.txt ServletInsertTeaching.java 0.15924970422098075
SMOS38.txt ServletShowJustifyDetails.java 0.6643619820378176
SMOS38.txt ServletDeleteAddress.java 0.15917517244874604
SMOS38.txt ServletShowNoteDetails.java 0.6473602108537946
SMOS38.txt Delay.java 0.14271420762519108
SMOS38.txt ServletShowTeachingList.java 0.7066184792755128
SMOS38.txt ServletUpdateUser.java 0.17714979409334047
SMOS38.txt TestRegister.java 0.15782856715719465
SMOS38.txt ServletShowUserClassroomForm.java 0.30170969305632606
# corpus_name: SMOS (5_0)
SMOS7.txt MandatoryFieldException.java 0.007302555997154949
SMOS7.txt ServletShowRegister.java 0.1479639080143388
SMOS7.txt PermissionException.java 0.28331276907249914
SMOS7.txt Role.java 0.28619401696091007
SMOS7.txt ServletShowTeachingList.java 0.214474588822436
SMOS7.txt ServletShowAddressList.java 0.7555942200407736
SMOS7.txt ServletRemoveTeachingAsTeacher.java 0.655845571405973
SMOS7.txt ManagerVotes.java 0.18130133520730451
SMOS7.txt ServletShowReports.java 0.2021155590710295
SMOS7.txt Classroom.java 0.6400799059082778
SMOS7.txt ServletShowUserList.java 0.7596168904506352
SMOS7.txt UserTest.java 0.1742269208374547
# corpus_name: SMOS (5_0)
SMOS49.txt ServletInsertJustify.java 0.16275115598728754
SMOS49.txt ServletUpdateUser.java 0.9997552792099595
SMOS49.txt ServletRemoveStudentClassroom.java 0.2545877321681973
SMOS49.txt ServletShowNoteList.java 0.19078997756290972
SMOS49.txt ServletInsertNewNote.java 0.1956125558218545
SMOS49.txt ManagerVotes.java 0.21004931668344784
SMOS49.txt ManagerTeaching.java 0.09902035399786653
SMOS49.txt AdaptorException.java 0.7074400989248155
SMOS49.txt ServletLoadReport.java 0.18902133275567595
# corpus_name: SMOS (5_0)
SMOS8.txt ServletDeleteJustify.java 0.3003025132134659
SMOS8.txt Teaching.java 0.15946572147677637
SMOS8.txt ServletShowStudentsByClass.java 0.3014522831979416
SMOS8.txt ServletLogin.java 0.26133145050833206
SMOS8.txt ServletDeleteClassroom.java 0.29191318633448016
SMOS8.txt ServletShowClassroomManagement.java 0.2800787941722349
SMOS8.txt ServletAssignStudentClassroom.java 0.7778761803209275
SMOS8.txt ServletInitialize.java 0.3385689864414074
SMOS8.txt MandatoryFieldException.java 0.2421769326025642
SMOS8.txt Classroom.java 0.6484227515889837
SMOS8.txt ServletInsertAddress.java 0.8649967751697434
# corpus_name: SMOS (5_0)
SMOS44.txt ServletDeleteNote.java 0.8351102739156177
SMOS44.txt DuplicatedEntityException.java 0.2338296511803683
SMOS44.txt ServletInsertTeaching.java 0.7323644480407177
SMOS44.txt ServletShowAddressDetails.java 0.25399284842373787
SMOS44.txt ServletReportTeachings.java 0.3263343656948558
SMOS44.txt ManagerAddress.java 0.39945533961807317
# corpus_name: SMOS (5_0)
SMOS12.txt Report.java 0.14798871061551835
SMOS12.txt ServletAlterPersonalDate.java 0.6704901149632991
SMOS12.txt ServletUpdateTeaching.java 2.907491480999668e-06
SMOS12.txt DeleteAdministratorException.java 0.8091807538224248
SMOS12.txt ServletAddRemoveTeachingsAsAddress.java 0.3264004167649907
SMOS12.txt ServletShowAddressList.java 0.19033703040975541
SMOS12.txt ManagerRegister.java 0.2143170674853423
SMOS12.txt ServletShowRegister.java 0.00020890931322697
SMOS12.txt ServletUpdateJustify.java 0.13624130700438009
SMOS12.txt ServletDeleteClassroom.java 0.14213148937491407
SMOS12.txt ServletShowTeachingList.java 0.1723036786540644
SMOS12.txt ServletProva.java 0.003736781183672226
# corpus_name: SMOS (5_0)
SMOS45.txt Note.java 0.17997911080517537
SMOS45.txt ServletComputateStatistics.java 0.16370865227749806
SMOS45.txt DefaultException.java 0.3269861120264422
SMOS45.txt ConnectionWrapper.java 0.18945153243689125
SMOS45.txt ServletAssignParentStudent.java 0.17625193323576308
SMOS45.txt ServletShowUserList.java 0.18713023639453943
SMOS45.txt ServletInsertUser.java 0.6668734166700778
SMOS45.txt ServletShowJustifyDetails.java 0.6479431900447742
# corpus_name: SMOS (5_0)
SMOS22.txt ServletShowTeacherTeachingFormByClass.java 0.32723137671915203
SMOS22.txt Address.java 0.2243681335193896
SMOS22.txt ServletLogin.java 0.6593944468137721
SMOS22.txt ServletShowAddressList.java 0.7174924972367752
SMOS22.txt ServletShowJustifyDetails.java 0.15485401547019062
SMOS22.txt MailUtility.java 0.2380512271682276
SMOS22.txt Note.java 0.10636060164149588
SMOS22.txt RegisterLine.java 0.14914190180961823
SMOS22.txt ServletUpdateUser.java 4.360842372472099e-08
SMOS22.txt ServletUpdateReport.java 0.15336239794162146
SMOS22.txt ServletUpdateTeaching.java 0.26542277506051953
SMOS22.txt ServletDeleteJustify.java 0.15267052690334565
SMOS22.txt ServletLoadTeachingList.java 0.2489853786491537
# corpus_name: SMOS (5_0)
SMOS34.txt display.java 0.1436576383583845
SMOS34.txt ServletShowUserTeachingFormByCourse.java 0.24105655617017704
SMOS34.txt ServletRemoveStudentClassroom.java 0.7060807747950422
SMOS34.txt ServletShowUserDetails.java 0.18851245028258745
SMOS34.txt Address.java 0.7745681112973041
SMOS34.txt User.java 0.1057574948849337
SMOS34.txt ControlConnection.java 0.3395950489695229
SMOS34.txt ServletInsertClassroom.java 0.18657811490761186
SMOS34.txt Note.java 0.7639332789843931
SMOS34.txt ConnectionPoolDataSource.java 0.3777137690342833
SMOS34.txt ManagerTeaching.java 0.18459482440782685
# corpus_name: SMOS (5_0)
SMOS54.txt ManagerAddress.java 0.2830206599888404
SMOS54.txt ServletShowNoteDetails.java 0.1963819602271786
SMOS54.txt TestRegister.java 0.34595726402347843
SMOS54.txt ManagerRegister.java 0.8822774383292527
SMOS54.txt Votes.java 3.877933626237878e-08
SMOS54.txt ServletAssignParentStudent.java 0.17894152540129016
SMOS54.txt ServletShowStudentsByClass.java 0.251083466877203
# corpus_name: SMOS (5_0)
SMOS50.txt ServletAssignParentStudent.java 0.7645620176102469
SMOS50.txt AdaptorException.java 0.005585124436344661
SMOS50.txt ServletShowUserTeachingFormByCourse.java 0.24352337512840985
SMOS50.txt Environment.java 0.343595007649701
SMOS50.txt Votes.java 0.6668595526448755
SMOS50.txt ServletInsertAddress.java 0.17749771130192724
SMOS50.txt ServletAddRemoveTeachingsAsAddress.java 0.1733793426535374
SMOS50.txt TestRegister.java 0.18714426636084322
SMOS50.txt ConnectionException.java 0.21698725638442456
SMOS50.txt ServletLogout.java 0.6800822493946516
# corpus_name: SMOS (5_0)
SMOS48.txt ServletLoadTeachingList.java 0.6888256793195181
SMOS48.txt AdaptorException.java 0.7064339108775904
SMOS48.txt ServletAddRemoveTeachingsAsAddress.java 0.1785195205437823
SMOS48.txt ServletDeleteUser.java 0.8349817015630633
SMOS48.txt ServletUpdateTeaching.java 0.17539510178692988
SMOS48.txt ServletAddTeachingAsTeacher.java 0.18063572340018877
SMOS48.txt TestRegister.java 0.15777172871975428
SMOS48.txt ServletDeleteTeaching.java 0.20479585742934756
SMOS48.txt ServletLogout.java 0.2431841918820203
SMOS48.txt Delay.java 0.1803076377251373
SMOS48.txt ConnectionException.java 0.20899909335025615
# corpus_name: SMOS (5_0)
SMOS27.txt Role.java 0.2757850113367951
SMOS27.txt ConnectionPoolDataSource.java 0.33438006613435184
SMOS27.txt ServletShowTeachingList.java 0.17742496196769045
SMOS27.txt Delay.java 0.7448639913183531
SMOS27.txt Classroom.java 0.4544728390285604
SMOS27.txt DeleteAdministratorException.java 0.08378063278670465
SMOS27.txt ServletShowTeacherTeachingFormByClass.java 0.3335943437383176
SMOS27.txt ServletLoadTeachingList.java 0.6888058006136737
SMOS27.txt Address.java 0.8110644874891139
SMOS27.txt TestRegister.java 0.00024008548311095143
# corpus_name: SMOS (5_0)
SMOS51.txt Environment.java 0.3778021126231679
SMOS51.txt ServletAddRemoveTeachingsAsAddress.java 0.17886248435300406
SMOS51.txt ControlConnection.java 0.31679312543145516
SMOS51.txt ServletShowClassroomList.java 0.16888705002054116
SMOS51.txt ServletInsertReport.java 0.03745618570531899
SMOS51.txt ServletShowReports.java 0.7314103039505835
SMOS51.txt ServletDeleteReport.java 0.3413990451948658
SMOS51.txt ServletShowTeachingList.java 0.09067989680705174
SMOS51.txt ServletShowUserList.java 0.6956790414868512
SMOS51.txt ServletAssignRole.java 0.7654438701362509
SMOS51.txt ServletComputateStatistics.java 0.23274662020805126
SMOS51.txt ServletShowNoteDetails.java 0.024742057623315872
SMOS51.txt ServletShowJustifyList.java 0.2387978929453443
SMOS51.txt UserListItem.java 0.6681194101396858
SMOS51.txt ConnectionPoolDataSource.java 0.31698019213917716
SMOS51.txt ConnectionException.java 0.7223376038953381
# corpus_name: SMOS (5_0)
SMOS3.txt User.java 0.745531336045505
SMOS3.txt Delay.java 0.1291784400574831
SMOS3.txt ServletShowRegister.java 0.02456434609042392
SMOS3.txt ServletShowClassroomList.java 0.040658882933498855
SMOS3.txt ServletReportTeachings.java 0.16094183735121
SMOS3.txt DuplicatedEntityException.java 0.14190798424263829
SMOS3.txt ServletInsertTeaching.java 0.12816515492130431
SMOS3.txt ServletAddressTeachings.java 0.17604192758677373
# corpus_name: SMOS (5_0)
SMOS42.txt ServletInitialize.java 0.3423146921843427
SMOS42.txt ConnectionPoolDataSource.java 0.2837247484960912
SMOS42.txt ServletAssignStudentClassroom.java 0.22090281985030658
SMOS42.txt ServletDeleteAddress.java 0.18341945147916144
SMOS42.txt ConnectionWrapper.java 0.14911469246133177
SMOS42.txt ServletLogout.java 0.17003957662599478
SMOS42.txt ServletShowTeachingList.java 0.1838192543853051
SMOS42.txt Teaching.java 0.25763870660307836
SMOS42.txt ServletInsertNewNote.java 0.1486803565495306
SMOS42.txt AdaptorException.java 0.7098558053855282
SMOS42.txt display.java 0.13633368608404317
# corpus_name: SMOS (5_0)
SMOS11.txt ServletDeleteClassroom.java 0.11032095221952677
SMOS11.txt ManagerAddress.java 0.25353351519848505
SMOS11.txt ServletLogin.java 0.15077642717860512
SMOS11.txt ManagerClassroom.java 0.25784166687322424
SMOS11.txt ManagerVotes.java 0.2390154700866724
SMOS11.txt ServletInitialize.java 0.2568786427619217
SMOS11.txt MandatoryFieldException.java 0.08627812128766482
SMOS11.txt Role.java 0.8521997191280952
SMOS11.txt ServletUpdateTeaching.java 0.2061687419126655
SMOS11.txt User.java 0.6668497868848343
SMOS11.txt AdaptorException.java 0.7644710032348566
SMOS11.txt ServletShowNoteDetails.java 0.6430637945247244
SMOS11.txt ServletAssignStudentClassroom.java 0.22756040091015098
SMOS11.txt ServletInsertNewNote.java 0.14422940436065038
# corpus_name: SMOS (5_0)
SMOS25.txt managerUser.java 0.44356785824370226
SMOS25.txt ManagerRegister.java 0.3194894693994838
SMOS25.txt ServletShowNoteDetails.java 0.2548369773289945
SMOS25.txt ServletInitialize.java 0.2999782680873549
SMOS25.txt DBConnection.java 0.27261050816072463
SMOS25.txt ServletShowNoteList.java 0.2593866973293567
SMOS25.txt ServletShowUserTeachingFormByCourse.java 0.7836779652438889
SMOS25.txt ServletRemoveStudentClassroom.java 0.2628572548627186
SMOS25.txt ControlConnection.java 0.891817877541821
SMOS25.txt ServletInsertJustify.java 0.3339452797001906
SMOS25.txt RegisterLine.java 0.16582095699298804
SMOS25.txt ServletShowAddressDetails.java 0.8299631498097315
SMOS25.txt ServletInsertTeaching.java 0.386904419956273
# corpus_name: SMOS (5_0)
SMOS14.txt NotImplementedYetException.java 0.1839676238134495
SMOS14.txt ConnectionException.java 0.37163198928924523
SMOS14.txt ServletLoadClassByAccademicYear.java 0.9102408186540507
SMOS14.txt ServletLoadTeachingList.java 0.3109119753766284
SMOS14.txt managerUser.java 0.37950679938289156
SMOS14.txt ServletShowJustifyList.java 0.29485090696064536
SMOS14.txt ServletDeleteJustify.java 0.2964121955918877
# corpus_name: SMOS (5_0)
SMOS56.txt ServletComputateStatistics.java 0.15189407277423028
SMOS56.txt ServletAssignParentStudent.java 0.11085663774937328
SMOS56.txt ServletInsertUser.java 0.6669096225233163
SMOS56.txt Classroom.java 0.9716336569741729
SMOS56.txt MandatoryFieldException.java 0.2692230676854407
SMOS56.txt ServletDeleteUser.java 0.2111127400044931
SMOS56.txt Report.java 0.2723401576842652
SMOS56.txt ServletShowJustifyDetails.java 0.10584173843151307
SMOS56.txt ServletLoadReport.java 0.689004043689386
# corpus_name: SMOS (5_0)
SMOS10.txt DuplicatedEntityException.java 0.13921663882708937
SMOS10.txt ServletAssignParentStudent.java 0.6306477540508396
SMOS10.txt ServletRemoveStudentClassroom.java 0.6351311580232347
SMOS10.txt ServletShowReports.java 2.1928458079979657e-05
SMOS10.txt ConnectionPoolDataSource.java 0.8111316388787241
SMOS10.txt ServletDeleteAddress.java 0.6684271585652602
SMOS10.txt UserListItem.java 0.008336356840311488
SMOS10.txt MailUtility.java 0.22144538414191942
SMOS10.txt TestRegister.java 0.9875794043427024
SMOS10.txt MandatoryFieldException.java 0.006735648834192781
SMOS10.txt LoginException.java 0.9383434070761266
# corpus_name: SMOS (5_0)
SMOS30.txt ServletRemoveStudentClassroom.java 0.18781808652597035
SMOS30.txt ServletDeleteJustify.java 0.660209038188209
SMOS30.txt ServletInsertAddress.java 0.7308257704024069
SMOS30.txt PermissionException.java 0.08100188251952213
SMOS30.txt DuplicatedEntityException.java 0.29366151877839564
SMOS30.txt UserListItem.java 0.00038258140278260545
SMOS30.txt ServletShowReports.java 0.19059764350916586
# corpus_name: SMOS (5_0)
SMOS55.txt ServletInsertUser.java 0.7303533460768245
SMOS55.txt UserListItem.java 0.22936107155147423
SMOS55.txt ServletInsertTeaching.java 0.2150014359777165
SMOS55.txt ServletAssignRole.java 0.2279816117317982
SMOS55.txt ServletShowUserTeachingForm.java 0.17345727729879112
SMOS55.txt NotImplementedYetException.java 0.039011698614244104
SMOS55.txt ServletUpdateTeaching.java 0.1605211271068646
SMOS55.txt Absence.java 0.30919658715861836
SMOS55.txt managerUser.java 0.34928217450827725
SMOS55.txt ServletDeleteNote.java 0.6558852792030045
SMOS55.txt DeleteAdministratorException.java 0.36312445614204103
# corpus_name: SMOS (5_0)
SMOS19.txt AdaptorException.java 0.7765419390672889
SMOS19.txt ServletInsertReport.java 0.2926690417285246
SMOS19.txt Note.java 3.013828264276525e-08
SMOS19.txt LoginException.java 0.2470126714143988
SMOS19.txt TestRegister.java 0.08688838321661148
SMOS19.txt NotImplementedYetException.java 0.6913842171540543
SMOS19.txt ServletShowUserRoleForm.java 0.20449862139446817
SMOS19.txt PermissionException.java 0.7082815340722538
SMOS19.txt ControlConnection.java 0.35940619792475775
SMOS19.txt ServletLoadClassByAccademicYear.java 0.17788620730622756
SMOS19.txt ServletReportTeachings.java 0.22389841373311017
SMOS19.txt ManagerClassroom.java 0.6728979739563956
# corpus_name: SMOS (5_0)
SMOS4.txt UserTest.java 0.010551691108834797
SMOS4.txt ServletLoadClassByAccademicYear.java 0.16440187430448905
SMOS4.txt ServletLogout.java 0.004481803853756515
SMOS4.txt ServletShowStudentsByClass.java 0.1666005343627137
SMOS4.txt ServletShowUserDetails.java 0.14543599153247735
SMOS4.txt PermissionException.java 0.20807223209506392
SMOS4.txt Utility.java 0.3060546287385296
SMOS4.txt ServletShowUserTeachingFormByCourse.java 0.8536775967118819
SMOS4.txt ConnectionPoolDataSource.java 0.8584881648946839
