# Comet Automated Traceability Link Recovery Framework

## Project Summary

Coming Soon

## Use Cases

Coming Soon

## Getting Started

### Prerequisites

Coming Soon

### Installing and Running Comet


## Usage

Coming Soon

### Parameters

Coming Soon

### Example Input Files

Coming Soon

### Usage Flags

Coming Soon

## Output

Coming Soon

## Our Team

### Advisors

* [Kevin Moran](http://www.kpmoran.com)
* [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html)
* Chris Shenefiel

### Graduate Student Contributors

* [David Nader Palacio](https://github.com/danaderp)
* [Carlos Bernal Cardenas](http://www.cs.wm.edu/~cebernal/)

### Undergraduate Contributors

* Daniel McCrystal

## Citation

If you utilize Comet in your academic work, we kindly ask that you cite our ICSE'20 paper:

> 
