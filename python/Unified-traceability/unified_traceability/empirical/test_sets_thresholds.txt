Evaluating: vsm (n = 2)
==============================
Testing whether min-max outperforms ['link_est', 'gt_sample', 'infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: min-max and compared technique have same distribution)
-----------------------------------------
min-max - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['gt_sample', 'infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['mean', 'sigmoid', 'median']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['sigmoid', 'median']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['median']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
min-max         mean: 0.932269   stdev: 0.051593
link_est        mean: 0.726712   stdev: 0.386488
gt_sample       mean: 0.718836   stdev: 0.227078
infer_samples   mean: 0.671217   stdev: 0.307114
mean            mean: 0.425239   stdev: 0.130470
sigmoid         mean: 0.405521   stdev: 0.126389
median          mean: 0.353243   stdev: 0.108168

Evaluating: lsi (n = 2)
==============================
Testing whether gt_sample outperforms ['link_est', 'infer_samples', 'median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['infer_samples', 'median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether median outperforms ['sigmoid', 'mean', 'min-max']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - sigmoid -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['mean', 'min-max']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['min-max']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
gt_sample       mean: 0.802890   stdev: 0.132723
link_est        mean: 0.698127   stdev: 0.073062
infer_samples   mean: 0.693996   stdev: 0.269125
median          mean: 0.660527   stdev: 0.356405
sigmoid         mean: 0.646952   stdev: 0.319253
mean            mean: 0.640931   stdev: 0.308551
min-max         mean: 0.532121   stdev: 0.205977

Evaluating: js (n = 2)
==============================
Testing whether gt_sample outperforms ['min-max', 'link_est', 'infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - min-max -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether min-max outperforms ['link_est', 'infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: min-max and compared technique have same distribution)
-----------------------------------------
min-max - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['infer_samples', 'mean', 'sigmoid', 'median']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['mean', 'sigmoid', 'median']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['sigmoid', 'median']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - median -> P-val: 0.654720846019 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['median']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - median -> P-val: 0.654720846019 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
gt_sample       mean: 0.754504   stdev: 0.212567
min-max         mean: 0.732753   stdev: 0.142687
link_est        mean: 0.730905   stdev: 0.307566
infer_samples   mean: 0.708723   stdev: 0.292960
mean            mean: 0.452980   stdev: 0.155900
sigmoid         mean: 0.449185   stdev: 0.158227
median          mean: 0.432592   stdev: 0.186262

Evaluating: lda (n = 2)
==============================
Testing whether infer_samples outperforms ['min-max', 'gt_sample', 'sigmoid', 'link_est', 'mean', 'median']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether min-max outperforms ['gt_sample', 'sigmoid', 'link_est', 'mean', 'median']
(Null Hypothesis: min-max and compared technique have same distribution)
-----------------------------------------
min-max - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['sigmoid', 'link_est', 'mean', 'median']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['link_est', 'mean', 'median']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
sigmoid - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['mean', 'median']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['median']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
infer_samples   mean: 0.591985   stdev: 0.348561
min-max         mean: 0.343351   stdev: 0.301569
gt_sample       mean: 0.209243   stdev: 0.315147
sigmoid         mean: 0.095620   stdev: 0.049590
link_est        mean: 0.095295   stdev: 0.035145
mean            mean: 0.095103   stdev: 0.049353
median          mean: 0.054292   stdev: 0.024320

Evaluating: nmf (n = 2)
==============================
Testing whether sigmoid outperforms ['gt_sample', 'link_est', 'infer_samples', 'median', 'mean', 'min-max']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['link_est', 'infer_samples', 'median', 'mean', 'min-max']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['infer_samples', 'median', 'mean', 'min-max']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['median', 'mean', 'min-max']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether median outperforms ['mean', 'min-max']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['min-max']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
sigmoid         mean: 0.879889   stdev: 0.000361
gt_sample       mean: 0.806924   stdev: 0.157426
link_est        mean: 0.797904   stdev: 0.024530
infer_samples   mean: 0.754414   stdev: 0.250395
median          mean: 0.658643   stdev: 0.269053
mean            mean: 0.641353   stdev: 0.321091
min-max         mean: 0.167865   stdev: 0.093539

Evaluating: jslda (n = 2)
==============================
Testing whether median outperforms ['sigmoid', 'mean', 'min-max', 'link_est', 'gt_sample', 'infer_samples']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - sigmoid -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - mean -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - min-max -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
median - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
median - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['mean', 'min-max', 'link_est', 'gt_sample', 'infer_samples']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
sigmoid - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['min-max', 'link_est', 'gt_sample', 'infer_samples']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
mean - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether min-max outperforms ['link_est', 'gt_sample', 'infer_samples']
(Null Hypothesis: min-max and compared technique have same distribution)
-----------------------------------------
min-max - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['gt_sample', 'infer_samples']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['infer_samples']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
median          mean: 0.903523   stdev: 0.054813
sigmoid         mean: 0.901747   stdev: 0.083403
mean            mean: 0.899184   stdev: 0.081730
min-max         mean: 0.877789   stdev: 0.108928
link_est        mean: 0.761257   stdev: 0.304302
gt_sample       mean: 0.697076   stdev: 0.238109
infer_samples   mean: 0.687759   stdev: 0.263622

Evaluating: jsnmf (n = 2)
==============================
Testing whether gt_sample outperforms ['link_est', 'infer_samples', 'mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['infer_samples', 'mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['sigmoid', 'median', 'min-max']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
mean - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['median', 'min-max']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - median -> P-val: 0.654720846019 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether median outperforms ['min-max']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
gt_sample       mean: 0.777423   stdev: 0.219703
link_est        mean: 0.738773   stdev: 0.323811
infer_samples   mean: 0.700483   stdev: 0.284152
mean            mean: 0.472115   stdev: 0.142125
sigmoid         mean: 0.463275   stdev: 0.154495
median          mean: 0.440442   stdev: 0.191447
min-max         mean: 0.090334   stdev: 0.035930

Evaluating: vsmjs (n = 2)
==============================
Testing whether min-max outperforms ['infer_samples', 'gt_sample', 'link_est', 'mean', 'sigmoid', 'median']
(Null Hypothesis: min-max and compared technique have same distribution)
-----------------------------------------
min-max - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - gt_sample -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
min-max - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
min-max - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['gt_sample', 'link_est', 'mean', 'sigmoid', 'median']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - link_est -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['link_est', 'mean', 'sigmoid', 'median']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['mean', 'sigmoid', 'median']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['sigmoid', 'median']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['median']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - median -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
min-max         mean: 0.769983   stdev: 0.072718
infer_samples   mean: 0.745537   stdev: 0.286302
gt_sample       mean: 0.729515   stdev: 0.242494
link_est        mean: 0.666738   stdev: 0.314371
mean            mean: 0.444154   stdev: 0.170947
sigmoid         mean: 0.434846   stdev: 0.167429
median          mean: 0.392384   stdev: 0.178794

Evaluating: vsmlda (n = 2)
==============================
Testing whether gt_sample outperforms ['link_est', 'infer_samples', 'median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - link_est -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether link_est outperforms ['infer_samples', 'median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['median', 'sigmoid', 'mean', 'min-max']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether median outperforms ['sigmoid', 'mean', 'min-max']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
median - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
median - min-max -> P-val: 0.654720846019 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['mean', 'min-max']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.654720846019 -> Difference is not statistically significant.

Testing whether mean outperforms ['min-max']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - min-max -> P-val: 0.654720846019 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
gt_sample       mean: 0.741728   stdev: 0.181655
link_est        mean: 0.727703   stdev: 0.160029
infer_samples   mean: 0.684780   stdev: 0.257968
median          mean: 0.487425   stdev: 0.184720
sigmoid         mean: 0.475515   stdev: 0.189294
mean            mean: 0.464266   stdev: 0.177452
min-max         mean: 0.458817   stdev: 0.102740

Evaluating: vsmnmf (n = 2)
==============================
Testing whether link_est outperforms ['infer_samples', 'gt_sample', 'mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: link_est and compared technique have same distribution)
-----------------------------------------
link_est - infer_samples -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
link_est - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
link_est - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether infer_samples outperforms ['gt_sample', 'mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: infer_samples and compared technique have same distribution)
-----------------------------------------
infer_samples - gt_sample -> P-val: 0.654720846019 -> Difference is not statistically significant.
infer_samples - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
infer_samples - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether gt_sample outperforms ['mean', 'sigmoid', 'median', 'min-max']
(Null Hypothesis: gt_sample and compared technique have same distribution)
-----------------------------------------
gt_sample - mean -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
gt_sample - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether mean outperforms ['sigmoid', 'median', 'min-max']
(Null Hypothesis: mean and compared technique have same distribution)
-----------------------------------------
mean - sigmoid -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
mean - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether sigmoid outperforms ['median', 'min-max']
(Null Hypothesis: sigmoid and compared technique have same distribution)
-----------------------------------------
sigmoid - median -> P-val: 0.179712494879 -> Difference is not statistically significant.
sigmoid - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Testing whether median outperforms ['min-max']
(Null Hypothesis: median and compared technique have same distribution)
-----------------------------------------
median - min-max -> P-val: 0.179712494879 -> Difference is not statistically significant.

Remaining potential techniques cannot be statistically proven to outperform one another. Recommend using the technique with the lowest standard deviation.

Results
-----------------------------
link_est        mean: 0.728758   stdev: 0.383594
infer_samples   mean: 0.676509   stdev: 0.295648
gt_sample       mean: 0.662192   stdev: 0.232466
mean            mean: 0.438408   stdev: 0.133583
sigmoid         mean: 0.415989   stdev: 0.128053
median          mean: 0.354492   stdev: 0.106401
min-max         mean: 0.069001   stdev: 0.015177
